from django.contrib import admin
# Import the model you wish to register
from recipes.models import Recipe, RecipeStep, Ingredient

# Register your models here.
@admin.register(Recipe)
# Register the model using a class that inherits
# from admin.ModelAdmin
class RecipeAdmin(admin.ModelAdmin):
    # Add a list_display property that is
    # a tuple of model properties that you'd
    # like to see in the admin list view
    list_display = (
        "title",
        "id",
    )

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )

@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = (
        "amount",
        "food_item",
        "recipe",
    )
