from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
# Import model form from app.forms
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def create_recipe(request):
    if request.method == "POST":
        # We should use the form to validate the values
        #   and save them to the database
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            # If all goes well, we can redirect the browser
            #   to another page and leave the function
            return redirect("recipe_list")
    else:
        # Create an instance of the Django model form class
        form = RecipeForm()

    # Create context and add form to it
    context = {
        "form": form,
    }
    # Render recipe form view
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    # Get the object that we want to edit
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        # Post is when the person has submitted the form
        form  = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)

    context = {
        "form": form,
        "recipe_object": recipe,
    }
    return render(request, "recipes/edit.html", context)

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
